﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CuttingPlateBehaviour : MonoBehaviour
{

    [SerializeField] Vegies inventory;

    [SerializeField] Sprite[] item;

    [SerializeField] Image inventoryImage;

    private void Start()
    {
        inventory = Vegies.non;
        SetItemOnPlate(inventory);
    }

    //reset
    public void Reset()
    {
        inventory = Vegies.non;
        SetItemOnPlate(inventory);
    }

    //Set Item
    public void SetItemOnPlate(Vegies value)
    {
        inventory = value;

        if(value!= Vegies.non)
        {
            inventoryImage.sprite = item[(int)value];
            inventoryImage.gameObject.SetActive(true);
        }
        else
        {
            inventoryImage.sprite = null;
            inventoryImage.gameObject.SetActive(false);
        }
    }


    //get item
    public Vegies GetItemonPlate()
    {
        return inventory;
    }
}
