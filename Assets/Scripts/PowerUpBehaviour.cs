﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBehaviour : MonoBehaviour
{
    [SerializeField] PowerUP power;

    [SerializeField] int points;

    [SerializeField] float addTime;

    [SerializeField] float powerUpTime;

    public Player playerType;

    [SerializeField] SpriteRenderer powerupImage;
    [SerializeField] Sprite star, coin, time;

    //Set up powerup object sprite
    private void OnEnable()
    {
        switch (power)
        {
            case PowerUP.time:
                powerupImage.sprite = time;
                break;
            case PowerUP.coin:
                powerupImage.sprite = coin;
                break;
            case PowerUP.star:
                powerupImage.sprite = star;
                break;
            default:
                break;
        }
    }

    //sends Score
    public int GetPoints()
    {
        return points;
    }

    //sends Time
    public float GetAddTime()
    {
        return addTime;
    }

    //sends powerUp time
    public float GetPowerUpTimer()
    {
        return powerUpTime;
    }

    //Sends collected power
    public PowerUP GetPower()
    {
        return power;
    }
}
