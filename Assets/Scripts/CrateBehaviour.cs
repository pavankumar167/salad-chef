﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateBehaviour : MonoBehaviour
{
    [SerializeField] Highliter highlight;
    [SerializeField] Vegies item;
    [SerializeField] Sprite itemSprite;
    [SerializeField] LookDirection interactionDirection;
 
    private void Start()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite = itemSprite;
        }
       
    }

    //sends vegetable in create
    public Vegies GetCrateItem()
    {
        if (highlight.ishighlighted)
        {
            return item;
        }
        else
        {
            return Vegies.non;
        }
    }
}
