﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Vegies { non, beetroot, carrot, greenTomatos, redTomatos, capsicum, mushroom}
public enum LookDirection { left, right, front, back}
public enum PowerUP { time, coin, star}

public enum Player { player1, player2}

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] bool ispowerUp;
    [SerializeField] bool saladPicked;
    [SerializeField] float totalTime;
    [SerializeField] int totalScore;

    [SerializeField] int wastageCost;
    [SerializeField] int perMealCost;
    [SerializeField] int customerLossCost;

    public Player playerType;
    [SerializeField] KeyCode pickUpKey;
    [SerializeField] LookDirection playerlookDirection = LookDirection.front;
    [SerializeField] List<Vegies> inventory;
    [SerializeField] List<Vegies> saladCombination;

    [SerializeField] Image[] inventoryImage;
    [SerializeField] Sprite[] item;
    [SerializeField] Text scoreText;
    [SerializeField] Text playerTime;
    
    [SerializeField] GameObject powerUpSlider;
    [SerializeField] GameObject timerOutHud;
    [SerializeField] GameObject playerUI;
    [SerializeField] GameObject interactingObj;
    
    Coroutine powerUpCorotien;

    void Start()
    {
        inventory = new List<Vegies>();
        
        foreach (var item in inventoryImage)
        {
            item.gameObject.SetActive(false);
        }
        playerlookDirection = LookDirection.front;
        saladPicked = false;
        ispowerUp = false;
        scoreText.text = totalScore.ToString();
        playerTime.text = totalTime.ToString();
        powerUpSlider.SetActive(false);
        timerOutHud.SetActive(false);
        
    }

    private void OnEnable()
    {
        playerUI.SetActive(true);
    }

    private void OnDisable()
    { 
        if (playerUI!=null)
            playerUI.SetActive(false);
    }


    //Reset 
    public void ResetPlayer()
    {
        playerlookDirection = LookDirection.front;
        saladPicked = false;
        ispowerUp = false;
        totalScore = 0;
        totalTime = 200;
        scoreText.text = totalScore.ToString();
        playerTime.text = totalTime.ToString();
        powerUpSlider.SetActive(false);
        timerOutHud.SetActive(false);
        playerUI.SetActive(true);
        GetComponent<PlayerMovement>().Reset();
        inventory.Clear();
        DisplayHud();
    }

    #region All player interaction 

    private void Update()
    {
        if (totalTime > 0)
        {
            CalculateTimer();

            if (Input.GetKeyDown(pickUpKey) && interactingObj != null)
            {
                //Interaction With Vegetable Creats
                if (interactingObj.GetComponent<CrateBehaviour>() != null && !saladPicked)
                {
                    CrateInteraction();
                }

                // Interaction with CuttingPlate
                else if (interactingObj.GetComponent<CuttingPlateBehaviour>() != null && !saladPicked)
                {
                    SmallPlateInteraction();
                }

                // Interaction With CuttingBoard
                else if (interactingObj.GetComponent<CuttingBoardBehaviour>() != null && !saladPicked)
                {
                    CuttingBoardInteraction();
                }

                // Interaction with Customer plate
                else if (interactingObj.GetComponent<CustomerPlateBehaviour>() != null && saladPicked && interactingObj.GetComponent<CustomerPlateBehaviour>().CheckforCustomer())
                {
                    CustomerPlateInteraction();
                }

                // Interaction with TrashCan
                else if (interactingObj.CompareTag("Trash"))
                {
                    TrashCanInteraction();
                }
            }
        }
        else
        {
            if (!timerOutHud.activeSelf)
            {                
                timerOutHud.SetActive(true);
                inventory.Clear();
                DisplayHud();
                GameController.Instance.SetHighScore(totalScore);
            }
            
        }
    }

    //player timer
    private void CalculateTimer()
    {
        totalTime = totalTime - Time.deltaTime;
        playerTime.text = Mathf.Ceil(totalTime).ToString();
    }

    void CrateInteraction()
    {
        if (inventory.Count < 2)
        {
            inventory.Add(interactingObj.GetComponent<CrateBehaviour>().GetCrateItem());
            DisplayHud();
        }
        else if (inventory.Count >= 2)
        {
            if (inventory[0] == interactingObj.GetComponent<CrateBehaviour>().GetCrateItem())
            {
                inventory.RemoveAt(0);
                DisplayHud();
            }
            else if (inventory[1] == interactingObj.GetComponent<CrateBehaviour>().GetCrateItem())
            {
                inventory.RemoveAt(1);
                DisplayHud();
            }
            else
            {
                Debug.Log("No Match Found");
            }
        }
    }

    void CuttingBoardInteraction()
    {
        if (inventory.Count > 0 && interactingObj.GetComponent<CuttingBoardBehaviour>().InventoryCount() < 3 && this.GetComponent<PlayerMovement>().GetCanMove())
        {

            interactingObj.GetComponent<CuttingBoardBehaviour>().AddforCutting(inventory[0], ispowerUp);
            this.GetComponent<PlayerMovement>().CuttingToggle();
            inventory.RemoveAt(0);
            DisplayHud();

        }
        else if (inventory.Count == 0 && this.GetComponent<PlayerMovement>().GetCanMove() && interactingObj.GetComponent<CuttingBoardBehaviour>().InventoryCount() > 0)
        {
            saladCombination.Clear();
            foreach (var item in interactingObj.GetComponent<CuttingBoardBehaviour>().GetCombination())
            {
                saladCombination.Add(item);
            }
            saladPicked = true;
            inventory.Add(Vegies.non);
            interactingObj.GetComponent<CuttingBoardBehaviour>().PickUpSalad();
            DisplayHud();
        }
    }

    void SmallPlateInteraction()
    {
        if (interactingObj.GetComponent<CuttingPlateBehaviour>().GetItemonPlate() == Vegies.non && inventory.Count > 0)
        {
            interactingObj.GetComponent<CuttingPlateBehaviour>().SetItemOnPlate(inventory[0]);
            inventory.RemoveAt(0);
            DisplayHud();
        }
        else if (interactingObj.GetComponent<CuttingPlateBehaviour>().GetItemonPlate() != Vegies.non && inventory.Count < 2)
        {
            inventory.Add(interactingObj.GetComponent<CuttingPlateBehaviour>().GetItemonPlate());
            interactingObj.GetComponent<CuttingPlateBehaviour>().SetItemOnPlate(Vegies.non);
            DisplayHud();
        }
        else
        {
            Debug.Log(interactingObj.GetComponent<CuttingPlateBehaviour>().GetItemonPlate() + ">>>" + inventory.Count);
        }
    }

    void CustomerPlateInteraction()
    {
        interactingObj.GetComponent<CustomerPlateBehaviour>().ServeSalad(saladCombination, this.gameObject);
        inventory.RemoveAt(0);
        DisplayHud();
        saladPicked = false;
    }

    void TrashCanInteraction()
    {
        if (inventory.Count > 0)
        {
            if (saladPicked)
            {
                Debug.Log("InTrash Wastage Cost " + saladCombination.Count * wastageCost);
                SetPlayerScore(saladCombination.Count * wastageCost);
            }
            else
            {
                Debug.Log("InTrash Wastage Cost " + wastageCost);
                SetPlayerScore(wastageCost);
            }
            inventory.RemoveAt(0);
            DisplayHud();
            saladPicked = false;
        }
    }

    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PowerUp"))
        {
            if (collision.GetComponent<PowerUpBehaviour>() != null )
            {
                if (collision.GetComponent<PowerUpBehaviour>().playerType == playerType)
                {
                    if (collision.GetComponent<PowerUpBehaviour>().GetPower() == PowerUP.coin)
                    {
                        totalScore += collision.GetComponent<PowerUpBehaviour>().GetPoints();
                        scoreText.text = totalScore.ToString();
                    }
                    else if (collision.GetComponent<PowerUpBehaviour>().GetPower() == PowerUP.time)
                    {
                        totalTime = totalTime + collision.GetComponent<PowerUpBehaviour>().GetAddTime();
                    }
                    else if (collision.GetComponent<PowerUpBehaviour>().GetPower() == PowerUP.star)
                    {
                        ispowerUp = true;
                        powerUpSlider.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
                        powerUpSlider.SetActive(true);
                        if (powerUpCorotien != null)
                        {
                            StopCoroutine(powerUpCorotien);
                        }
                        powerUpCorotien = StartCoroutine(PowerUpPlayerCountDown(collision.GetComponent<PowerUpBehaviour>().GetPowerUpTimer()));
                    }
                    collision.gameObject.SetActive(false);
                }
            }

        }
        else
        {
            interactingObj = collision.gameObject;
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        interactingObj = null;
    }

    IEnumerator PowerUpPlayerCountDown(float poweruptime)
    {
        
        float timer = poweruptime;
        while (timer > 0)
        {
            powerUpSlider.transform.GetChild(1).GetComponent<Image>().fillAmount = (timer / poweruptime);
            yield return new WaitForSeconds(0.1f);
            timer = timer- 0.1f;
        }
        powerUpSlider.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
        powerUpSlider.SetActive(false);
        powerUpSlider.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
        ispowerUp = false;
    }

    //// Display items on plate
    void DisplayHud()
    {
        foreach (var item in inventoryImage)
        {
            item.gameObject.SetActive(false);
        }
        
        for (int i = 0; i < inventory.Count; i++)
        {
            inventoryImage[i].gameObject.SetActive(true);
            inventoryImage[i].sprite = item[(int)inventory[i]];
        }
        
    }
    
    void SetPlayerScore(int val)
    {
        if (totalTime > 0)
        {
            totalScore += val;
            if (totalScore < 0)
            {
                totalScore = 0;
            }
            scoreText.text = totalScore.ToString();
        }
    }


    #region PUBLC calls
    public float GetPlayerTimer()
    {
        return totalTime;
    }

    public void SetLookDirection(LookDirection dir)
    {
        playerlookDirection = dir;
    }

    public bool IsLookDirection(LookDirection dir)
    {
        if (playerlookDirection == dir)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsPowerUpPicked()
    {
        if (ispowerUp)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void FoodServedResult(bool value)
    {
        if (value)
        {
            SetPlayerScore(perMealCost);
        }
        else
        {
            SetPlayerScore(customerLossCost);
        }
    }

    public int GetScore()
    {
        return totalScore;
    }

    #endregion

}