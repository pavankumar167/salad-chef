﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CustomerPlateBehaviour : MonoBehaviour
{
    [SerializeField] List<Vegies> inventory;

    [SerializeField] Sprite[] item;

    [SerializeField] Image[] inventoryImage;

    [SerializeField] GameObject powerUpStar;
    [SerializeField] GameObject powerUpCoin;
    [SerializeField] GameObject powerUpTime;


    [SerializeField] GameObject customerObj;
    [SerializeField] GameObject playerObj;
    
    void Start()
    {
        DisplayHud();
        powerUpStar.SetActive(false);
        powerUpCoin.SetActive(false);
        powerUpTime.SetActive(false);
    }

    public void Reset()
    {
        inventory.Clear();
        DisplayHud();
        powerUpStar.SetActive(false);
        powerUpCoin.SetActive(false);
        powerUpTime.SetActive(false);
    }


   

    //Show customers order on plate
    public void SetPlateOrder(List<Vegies> order)
    {
        inventory.Clear();
        foreach (var item in order)
        {
            inventory.Add(item);
        }
        DisplayHud();
    }

    #region Verify and validate service

    public void ServeSalad(List<Vegies> combi, GameObject player)
    {
        foreach (var item in inventoryImage)
        {
            item.gameObject.SetActive(false);
        }
        inventoryImage[0].sprite = item[0];
        VerifyCustomer(combi,player);
    }

    void VerifyCustomer(List<Vegies> combi, GameObject player)
    {
        bool answer = Verify(inventory, combi);
        StartCoroutine(Validate(answer,player));
    }

    bool Verify(List<Vegies> customerOrder, List<Vegies> servedOrder)
    {
        if (customerOrder.Count != servedOrder.Count)
        {
            return false;
        }
        else
        {
            int count = 0;
            var val = customerOrder.All(servedOrder.Contains);
            return val;
        }
    }

    IEnumerator Validate(bool answer, GameObject player)
    {
        customerObj.GetComponent<CustomerBehaviour>().SetPauseTimer();
        yield return new WaitForSeconds(1f);
        if (answer)
        {
            var percent = customerObj.GetComponent<CustomerBehaviour>().GetWaitTimePrecentage();
            if ((percent * 100) >= 70)
            {
                SpawnPowerUp(player);
                customerObj.GetComponent<CustomerBehaviour>().SetCustomerCorrectAnswer(5);
            }
            else if((percent * 100) > 0)
            {
                customerObj.GetComponent<CustomerBehaviour>().SetCustomerCorrectAnswer(4);

            }
            player.GetComponent<PlayerBehaviour>().FoodServedResult(true);
            inventory.Clear();
            DisplayHud();
        }
        else
        {
            player.GetComponent<PlayerBehaviour>().FoodServedResult(false);
            customerObj.GetComponent<CustomerBehaviour>().SetPauseTimer();
            customerObj.GetComponent<CustomerBehaviour>().SetCustomerAngre(true);
            DisplayHud();
        }
    }

    void SpawnPowerUp(GameObject player)
    {
        int randonPowerUp = Random.Range(0, 3);
                switch ((PowerUP)randonPowerUp)
                {
                    case PowerUP.time:
                        powerUpTime.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        while(Vector2.Distance(powerUpTime.transform.position, player.transform.position) <= 2)
                        {
                            powerUpTime.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        }
                        powerUpTime.GetComponent<PowerUpBehaviour>().playerType = player.GetComponent<PlayerBehaviour>().playerType;
                        powerUpTime.SetActive(true);
                        break;
                    case PowerUP.coin:
                        powerUpCoin.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        while (Vector2.Distance(powerUpCoin.transform.position, player.transform.position) <= 2)
                        {
                            powerUpCoin.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        }
                        powerUpCoin.GetComponent<PowerUpBehaviour>().playerType = player.GetComponent<PlayerBehaviour>().playerType;
                        powerUpCoin.SetActive(true);
                        break;
                    case PowerUP.star:
                        powerUpStar.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        while (Vector2.Distance(powerUpStar.transform.position, player.transform.position) <= 2)
                        {
                            powerUpStar.transform.position = new Vector2(Random.Range(-6.0f, 6.0f), Random.Range(-4.0f, 0.0f));
                        }
                        powerUpStar.GetComponent<PowerUpBehaviour>().playerType = player.GetComponent<PlayerBehaviour>().playerType;
                        powerUpStar.SetActive(true);
                        break;
                    default:
                        break;
                }
    }

    #endregion


   // Display items on plate
    void DisplayHud()
    {
        foreach (var item in inventoryImage)
        {
            item.gameObject.SetActive(false);
        }
        for (int i = 0; i < inventory.Count; i++)
        {
            inventoryImage[i].gameObject.SetActive(true);
            inventoryImage[i].sprite = item[(int)inventory[i]];
        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Customer"))
        {
            collision.gameObject.GetComponent<CustomerBehaviour>().SetRandomOrder();
            inventory.Clear();
            customerObj = collision.gameObject;
            foreach (var item in collision.gameObject.GetComponent<CustomerBehaviour>().GetOrder())
            {
                inventory.Add(item);
            }
            DisplayHud();
            collision.gameObject.GetComponent<CustomerBehaviour>().StartWaitTimerCounter();
        }
        if (collision.CompareTag("Player"))
        {
            playerObj = collision.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Customer"))
        {
            customerObj = null;
            inventory.Clear();
            DisplayHud();
        }
        if (collision.CompareTag("Player"))
        {
            playerObj = null;
        }
    }

    //Check if customer exits
    public bool CheckforCustomer()
    {
        if (customerObj != null)
        {
            return true;
        }
        return false;
    }

}
