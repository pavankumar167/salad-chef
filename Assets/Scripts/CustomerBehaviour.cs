﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerBehaviour : MonoBehaviour
{
    [SerializeField] float waitTime;
    [SerializeField] float timeToServeOneVegetable;

    [SerializeField] List<Vegies> inventory;

    [SerializeField] GameObject waitTimerSlider;

    [SerializeField] GameObject player1;
    [SerializeField] GameObject player2;

    [SerializeField] Sprite[] emotionSprites;

    [SerializeField] Image emotionImmage;
 
    [SerializeField] bool pauseTimer;

    [SerializeField] bool isAngre;

    [SerializeField] bool isServed;

    private void Start()
    {
        pauseTimer = true;
        waitTimerSlider.SetActive(false);
    }


    //Set customer order
    public void SetRandomOrder()
    {
        int itemCount = Random.Range(1, 4);
        waitTime = timeToServeOneVegetable * itemCount;
        inventory.Clear();
        for (int i = 0; i < itemCount; i++)
        {
            inventory.Add((Vegies)Random.Range(1, 7));
        }
    }

    //get customer order
    public List<Vegies> GetOrder()
    {
        return inventory;
    }

    //Start customer wait time
    public void StartWaitTimerCounter()
    {
        isServed = false;
        emotionImmage.sprite = emotionSprites[0];
        waitTime = timeToServeOneVegetable * inventory.Count;
        waitTimerSlider.SetActive(true);
        pauseTimer = false;
        
    }

    //Reset and disable customer 
    public void DisableCustomer()
    {
        pauseTimer = true;
        waitTime = 0;
        this.gameObject.SetActive(false);
        this.GetComponent<CustomerMovement>().ResetPosition();
    }


    //Customer wait and update customer emoji
    private void Update()
    {
        if (!pauseTimer)
        {
            if (waitTime > 0)
            {
                if(isAngre)
                    waitTime = waitTime - (Time.deltaTime*3);
                else
                    waitTime = waitTime - Time.deltaTime;

                waitTimerSlider.transform.GetChild(1).GetComponent<Image>().fillAmount = (waitTime / (timeToServeOneVegetable * inventory.Count));
                if (!isAngre)
                {
                    if (waitTimerSlider.transform.GetChild(1).GetComponent<Image>().fillAmount >= 0.7f)
                    {
                        emotionImmage.sprite = emotionSprites[0];
                    }
                    else if (waitTimerSlider.transform.GetChild(1).GetComponent<Image>().fillAmount < 0.7f && waitTimerSlider.transform.GetChild(1).GetComponent<Image>().fillAmount >= 0.4f)
                    {
                        emotionImmage.sprite = emotionSprites[1];
                    }
                    else
                    {

                        emotionImmage.sprite = emotionSprites[2];
                    }
                }
            }
            else
            {
                pauseTimer = true;
            }
            
        }
        else
        {
            if(waitTime<=0)
            {
                isAngre = false;
                waitTimerSlider.SetActive(false);
                GetComponent<CustomerMovement>().MoveCustomer(1);
            }
        }
    }

    

    public void SetPauseTimer()
    {
        if (pauseTimer)
        {
            pauseTimer = false;
        }
        else
        {
            pauseTimer = true;
        }
    }

   // wrong answer reaction 
    public void SetCustomerAngre(bool value)
    {
        if (isAngre)
        {
            emotionImmage.sprite = emotionSprites[6];
            waitTime = 0;

        }
        else
        {
            isAngre = value;
            emotionImmage.sprite = emotionSprites[3];
        }
    }

    //Set customer reaction 
    public void SetCustomerCorrectAnswer(int emotion)
    {
        isServed = true;
        waitTime = 0;
        emotionImmage.sprite = emotionSprites[emotion];
    }

    //send percent of time customer waited
    public float GetWaitTimePrecentage()
    {
        return (waitTime/(timeToServeOneVegetable*inventory.Count));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Respawn"))
        {
            GetComponent<CustomerMovement>().RandomSpawn();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("CustomerPlate"))
        {
            if(!isServed)
            {
                if (player1.activeSelf)
                {
                    player1.GetComponent<PlayerBehaviour>().FoodServedResult(false);
                }
                if (player2.activeSelf)
                {
                    player2.GetComponent<PlayerBehaviour>().FoodServedResult(false);
                }
            }
        }
    }
}
