﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerMovement : MonoBehaviour
{
    Rigidbody2D rigidbody;

    [SerializeField] int customerID;

    [SerializeField] Vector2 startPos;
    [SerializeField] Vector2 finalPos;
    [SerializeField] float moveSpeed;

    [SerializeField] int moveDirection;

    SpriteRenderer rendrer;
    [SerializeField] Sprite[] customerFront;
    [SerializeField] Sprite[] customerBack;

    

    private void Start()
    {
        moveDirection = 0;
        rendrer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<Rigidbody2D>();
        
    }

    //Calls Spawncutomer at random time
    public void RandomSpawn()
    {
        Invoke("SpawnCustomer", Random.Range(1, 6));
    }

    //Sets random customer, customer movement, and customer order
    void SpawnCustomer()
    {
        GetComponent<CustomerBehaviour>().SetCustomerCorrectAnswer(0);
        customerID = Random.Range(0, customerFront.Length);
        MoveCustomer(-1);
        this.GetComponent<CustomerBehaviour>().SetRandomOrder();
    }

    //Reset customer poition
    public void ResetPosition()
    {
        this.transform.localPosition = startPos;
    }

    #region Customer movement
    private void FixedUpdate()
    {
        // move customr to the table 
        if(moveDirection == -1)
        {
            if (rendrer.sprite != customerFront[customerID])
            {
                rendrer.sprite = customerFront[customerID];
            }
            if (Vector2.Distance(transform.position, finalPos) > 0.1f)
            {
                rigidbody.MovePosition(rigidbody.position + (new Vector2(0,-1) * moveSpeed * Time.fixedDeltaTime));
            }
            else
            {
                moveDirection = 0;
            }

        }

        // move customer off the table
        else if(moveDirection == 1)
        {
            if (rendrer.sprite != customerBack[customerID])
            {
                rendrer.sprite = customerBack[customerID];
            }
            if (Vector2.Distance(transform.position, startPos) > 0.1f)
            {
                rigidbody.MovePosition(rigidbody.position + (new Vector2(0, 1) * moveSpeed * Time.fixedDeltaTime));
            }
            else
            {
                moveDirection = 0;
            }
        }
    }


    public void MoveCustomer(int val)
    {
        moveDirection = val;
    }
    #endregion


}
