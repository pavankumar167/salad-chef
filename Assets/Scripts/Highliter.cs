﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D),typeof(SpriteRenderer))]
public class Highliter : MonoBehaviour
{
    public bool ishighlighted;

    private void Awake()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(0.73f, 0.73f, 0.73f, 1f);
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            ishighlighted = true;
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            ishighlighted = false;
            this.GetComponent<SpriteRenderer>().color = new Color(0.73f, 0.73f, 0.73f, 1f);
        }
    }
}
