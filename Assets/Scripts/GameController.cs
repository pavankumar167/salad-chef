﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class HighscoreList
{
    public List<PlayerScore> scores;

    public HighscoreList()
    {
       scores = new List<PlayerScore>();
    }
}
[System.Serializable]
public class PlayerScore
{
    public int score;
}

public class GameController : MonoBehaviour
{
    private static GameController instance;
    public static GameController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<GameController>();
            return instance;
        }
    }


    private bool gameover;
    public bool Gameover
    {
        get
        {
            return gameover;
        }
    }

    [SerializeField] HighscoreList highscore;

    [SerializeField] int numberofTopScores;
    [SerializeField] int gamemode;


    [SerializeField] PlayerBehaviour player1;
    [SerializeField] PlayerBehaviour player2;
  
    [SerializeField] CustomerPlateBehaviour[] servingPlate;
   
    [SerializeField] CustomerBehaviour[] customer;
   
    [SerializeField] CuttingBoardBehaviour[] cuttingboard;
    [SerializeField] CuttingPlateBehaviour[] cuttingPlate;
    
    [SerializeField] GameObject mainmenPanel;
    [SerializeField] GameObject gameoverPanel;
    [SerializeField] Text[] highscoreText;

    [SerializeField] GameObject player1WinImage;
    [SerializeField] GameObject player2WinImage;

    private void Start()
    {
        gameover = false;
        //PlayerPrefs.DeleteAll();
        highscore = new HighscoreList();
    }

    //Set Gamemode 1player or 2 player
    public void SetGamemode(int players)
    {
        gamemode = players;
    }

    //Check if both players timer is 0 to set Gameover condition
    private void Update()
    {
        if (player1.gameObject.activeSelf && player2.gameObject.activeSelf && player1.GetPlayerTimer() <= 0 && player2.GetPlayerTimer() <= 0 && gameover == false)
        {
            GameOverSetup();
        }
        else if (player1.gameObject.activeSelf && player1.GetComponent<PlayerBehaviour>().GetPlayerTimer() <= 0 && !player2.gameObject.activeSelf && gameover == false)
        {
            GameOverSetup();
        }
    }

    #region Gameover and restart
    //Sets Gameover Screen
    void GameOverSetup()
    {
        if(!gameover)
        {
            gameover = true;
            gameoverPanel.SetActive(true);
            StopAllCoroutines();
            foreach (var item in servingPlate)
            {
                item.gameObject.SetActive(false);
            }
            foreach (var item in customer)
            {
                item.DisableCustomer();
            }
            foreach (var item in cuttingPlate)
            {
                item.Reset();
            }
            foreach (var item in cuttingboard)
            {
                item.Reset();
            }
        }
    }
    
    //Reset Gameelements for new game
    public void RestartGame()
    {
        player1WinImage.SetActive(false);
        player2WinImage.SetActive(false);
        player1WinImage.transform.GetChild(0).gameObject.SetActive(false);
        player2WinImage.transform.GetChild(0).gameObject.SetActive(false);
        if (gamemode == 1)
        {
            player1.gameObject.SetActive(true);
            player1.ResetPlayer();
            for (int i = 1; i < servingPlate.Length - 1; i++)
            {
                servingPlate[i].gameObject.SetActive(true);
                servingPlate[i].Reset();
                customer[i].gameObject.SetActive(true);
            }
        }
        else if (gamemode == 2)
        {
            player1.gameObject.SetActive(true);
            player2.gameObject.SetActive(true);
            player1.ResetPlayer();
            player2.ResetPlayer();
            for (int i = 0; i < servingPlate.Length; i++)
            {
                servingPlate[i].gameObject.SetActive(true);
                servingPlate[i].Reset();
                customer[i].gameObject.SetActive(true);
            }
        }
        gameover = false;
        gameoverPanel.SetActive(false);
    }
    #endregion

    #region Player HighScore Save and Load
    void GetHighScores()
    {
        string val = PlayerPrefs.GetString("HighScores");
        if (val.Length > 0)
        {
            highscore = new HighscoreList();
            highscore = JsonUtility.FromJson<HighscoreList>(val);
        }
    }

    public void SetHighScore(int playerScore)
    {
        GetHighScores();
        PlayerScore temp = new PlayerScore();
        temp.score = playerScore;
        highscore.scores.Add(temp);

        string val = JsonUtility.ToJson(highscore);
        PlayerPrefs.SetString("HighScores", val);
        PlayerPrefs.Save();
        DisplayTopScores();
    }

    void DisplayTopScores()
    {
        GetHighScores();

        if(gamemode == 2)
        {
            player1WinImage.SetActive(true);
            player2WinImage.SetActive(true);
            if (player1.GetScore() > player2.GetScore())
            {
                player1WinImage.transform.GetChild(0).gameObject.SetActive(true);
                player2WinImage.transform.GetChild(0).gameObject.SetActive(false);

            }
            else
            {
                player1WinImage.transform.GetChild(0).gameObject.SetActive(false);
                player2WinImage.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        foreach (var item in highscoreText)
        {
            item.text = "---";
        }
        SortHighScore(highscore.scores);
        if (highscore.scores.Count > numberofTopScores)
        {
            for (int i = 0; i < numberofTopScores; i++)
            {
                highscoreText[i].text = (i + 1) + " . " + highscore.scores[i].score.ToString();
            }
        }
        else
        {
            for (int i = 0; i < highscore.scores.Count; i++)
            {
                highscoreText[i].text = (i + 1) + " . " + highscore.scores[i].score.ToString();
            }
        }
    }

    void SortHighScore(List<PlayerScore> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                if (list[i].score < list[j].score)
                {
                    PlayerScore temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }
    }
    #endregion

}
