﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CuttingBoardBehaviour : MonoBehaviour
{

    [SerializeField] List<Vegies> inventory;

    [SerializeField] Sprite[] item;

    [SerializeField] Image[] inventoryImage;

    [SerializeField] AudioSource src;

    [SerializeField] AudioClip cut, cutFast;

    void Start()
    {
        inventory = new List<Vegies>();
        DisplayHud();
    }

    //reset
    public void Reset()
    {
        inventory = new List<Vegies>();
        DisplayHud();
    }

    //Starts Cutting
    public void AddforCutting(Vegies value, bool isPowerUp)
    {
        if (inventory.Count < 3)
        {
            src.Stop();
            src.clip = isPowerUp == false ? cut : cutFast;
            src.Play();
            inventory.Add(value);
            DisplayHud();
        }
    }

    //Sends combination cut by player
    public List<Vegies> GetCombination()
    {
        return inventory;
    }

    public int InventoryCount()
    {
        return inventory.Count;
    }

    //Clears cutting board
    public void PickUpSalad()
    {
        inventory.Clear();
        DisplayHud();
    }


    // Display items on plate
    void DisplayHud()
    {
        foreach (var item in inventoryImage)
        {
            item.gameObject.SetActive(false);
        }
        for (int i = 0; i < inventory.Count; i++)
        {
            inventoryImage[i].gameObject.SetActive(true);
            inventoryImage[i].sprite = item[(int)inventory[i]];
        }
    }
}
