﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rigidbody;
    SpriteRenderer rendrer;
    PlayerBehaviour behaviour;
    Vector2 movement;
    

    [Header("Movement Attributes")]
    [SerializeField] Vector2 initPosition;
    [SerializeField] KeyCode left, right, up, down;

    

    [SerializeField] float moveSpeed;
    [SerializeField] float powerUpSpeed;
    [SerializeField] bool canMove;
    [SerializeField] float cuttingTime;

    [Header("Player Rendrer Attributes")]
    [SerializeField] Sprite playerFront;
    [SerializeField] Sprite playerBack, playerLeft, playerRight;


    void Start()
    {
        rendrer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<Rigidbody2D>();
        behaviour = GetComponent<PlayerBehaviour>();

        rendrer.sprite = playerFront;
        canMove = true;
    }

    //reset
    public void Reset()
    {
        canMove = true;
        this.transform.position = initPosition;
        movement = Vector2.zero;
        rendrer.sprite = playerFront;
        behaviour.SetLookDirection(LookDirection.front);
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    private void Update()
    {
        if (canMove && behaviour.GetPlayerTimer()>0)
        {
            //keyDown Action
            if (Input.GetKeyDown(left))
            {
                rendrer.sprite = playerLeft;
                behaviour.SetLookDirection(LookDirection.left);
            }
            else if (Input.GetKeyDown(right))
            {
                rendrer.sprite = playerRight;
                behaviour.SetLookDirection(LookDirection.right);
            }
            else if (Input.GetKeyDown(up))
            {
                rendrer.sprite = playerBack;
                behaviour.SetLookDirection(LookDirection.back);
            }
            else if (Input.GetKeyDown(down))
            {
                rendrer.sprite = playerFront;
                behaviour.SetLookDirection(LookDirection.front);
            }

            //getKey Action
            if (Input.GetKey(left))
            {
                movement.x = -1;
                if (rendrer.sprite != playerLeft && !Input.GetKey(right))
                {
                    rendrer.sprite = playerLeft;
                    behaviour.SetLookDirection(LookDirection.left);
                }
            }
            else if (Input.GetKey(right))
            {
                movement.x = 1;
                if (rendrer.sprite != playerRight && !Input.GetKey(left))
                {
                    rendrer.sprite = playerRight;
                    behaviour.SetLookDirection(LookDirection.right);
                }
            }
            else if (Input.GetKey(up))
            {
                movement.y = 1;
                if (rendrer.sprite != playerBack && !Input.GetKey(down))
                {
                    rendrer.sprite = playerBack;
                    behaviour.SetLookDirection(LookDirection.back);
                }
            }
            else if (Input.GetKey(down))
            {
                movement.y = -1;
                if (rendrer.sprite != playerFront && !Input.GetKey(up))
                {
                    rendrer.sprite = playerFront;
                    behaviour.SetLookDirection(LookDirection.front);
                }
            }


            //keyUp Action
            if (Input.GetKeyUp(left))
            {
                movement.x = 0;
            }
            else if (Input.GetKeyUp(right))
            {
                movement.x = 0;
            }
            else if (Input.GetKeyUp(up))
            {
                movement.y = 0;
            }
            else if (Input.GetKeyUp(down))
            {
                movement.y = 0;
            }
        }
        else
        {
            if (behaviour.GetPlayerTimer() <= 0)
            {
                this.transform.position = initPosition;
                this.GetComponent<BoxCollider2D>().enabled = false;
            }
            movement = Vector2.zero;
            rendrer.sprite = playerFront;
            behaviour.SetLookDirection(LookDirection.front);
        }
    }


    void FixedUpdate()
    {
        //player movement
        if (canMove && movement!=Vector2.zero)
        {
            float speed = moveSpeed;
            if (behaviour.IsPowerUpPicked())
            {
                speed += powerUpSpeed;
            }

            rigidbody.MovePosition(rigidbody.position + (movement * speed * Time.fixedDeltaTime));
        }
    }
    
    //pause for vegitable cuttting
    public void CuttingToggle()
    {
        if (canMove)
        {
            canMove = false;
            if (behaviour.IsPowerUpPicked())
            {
                Invoke("CuttingToggle", cuttingTime - 1);
            }
            else
            {
                Invoke("CuttingToggle", cuttingTime);
            }
        }
        else
        {
            canMove = true;
           
        }
        
    }

    public bool GetCanMove()
    {
        return canMove;
    }

}
