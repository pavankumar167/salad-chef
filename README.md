# Salad Chef

Salad Chef is a co-op cooking game from one to two players. Chefs have to cook a delicious salad for all the customers as per there request before they walk off dissatisfied within the given time.

## Tools Used

* Unity version 2018.4.4f1
* Visual Studio Code
* Adobe PhotoShop
* Audacity

## Game Features

* Static Topdown view
* There are 6 different sets vegetables used in the game (Beetroot, Carrot, GreenTomato, RedTomato, Capsicum, Mushroom).
* Game can be played by both the player or single player. 
* There will be 5 customers if its 2 player game and 3 customers if its 1 player.
* Player Controle Keyboard input
	* Player 1: W, A, S, D for Up, Left, Down and Right movement.
	* Player 2: UpArrow, LeftArrow, DownArrow, RightArrow for Up, Left, Down and Right movement.
	* Player 1: Spacebar for pick up and drop interaction.
	* Player 2: Right Control for pick up and drop interaction.
* Players can carry only 2 vegetables or 1 salad at a time.
* To pick a vegetable Player must move near the crate and use the interaction key to pick up.
* Once the vegetables are picked player can cut the vegetables on the cutting board placed next to the trash can to create a combination.
* Player will not be able to move or perform any action until the cutting is completed.
* Player can also place one vegetable on the plate next to the cutting board.
* Player can also discard vegetable or the salad combination into the trash but would lose points for it.
* Players have to match the order by the customer shown on their plate (the combination need not be in the same order as shown).
* Player can pick up the salad once the combination is matching and serve it to the customer.
* Customer will have a waiting time depending on the order.
* If the customer waiting time ends then the customer will walk away players will loos points.
* If the customer gets the correct combination player is awarded points.
* If the customer gets the wrong combination then the player will lose points. If the customer is served wrong combination for the second time them customer walks away and both players lose points.
* If the customer is served before 70%(indicated with emoji in customers HUD) of the waiting time then there will be a powerup given to the player who served (can be only picked up by that player) and will pop up on a random position in the kitchen.
* Players powerups are as follows:
	* Star: Increases the player's movement speed for a period of time.
	* Time: Increases the overall time that the player has left.
	* Coin: Adds some points to the player score.
* Score is calculations are as follows
	* Wastagecost: -2 (eg: salad combination of 2 vegetables is thrown into trash player loses -4 points)
	* PerMealCost: 10 (serving correct combination to the customer player will gain 10 points).
	* CustomerlossCost: -5 (customer getting the wrong combination, the customer walks away after the waiting time would reduce -5 points from players score).

## Improvements 

* Adding better graphics with animation for all the characters in the game.
* Adding sounds for different game actions and game background.
* Introducing cooked food into the game(eg: player have to cook the food for a correct time while doing other tasks. Cooking for more or less customer would not accept the food).
* Make the game online multiplayer for more than 2 players(more players with an increased customer order).
* Introducing hurdle in the game (eg: a rat walks to the cutting board and picks up the cut vegetable. So player should stop the other work and get rid of the rat).

## Assets downloaded and referenced links

* [Game UI](https://opengameart.org/content/fruit-and-vegetables).
* [Emoji](https://www.freepik.com/free-vector/variety-flat-emoticons_1001238.htm#page=1&query=smiley&position=48).
* [Vegetables](https://www.cleanpng.com/png-vegetable-pixel-art-fruit-vegetables-867342/).
* [Chef](https://depositphotos.com/108461578/stock-illustration-pixel-chef-avatar-guy.html) referenced.
* [Customers](https://images.app.goo.gl/QvKUG4rGZCMLanB77) reference.
* [Trashcan](http://pixelartmaker.com/art/2ea0cdde0e78544)
* [SaladBowl](https://previews.123rf.com/images/saphatthachat/saphatthachat1806/saphatthachat180600149/103961335-vector-pixel-art-mix-salad-isolated-cartoon.jpg) reference.
* [fonts](https://www.1001fonts.com/world-of-water-font.html).
* [Game Background](http://hello.eboy.com/eboy/2017/09/26/tenkazushi-sushi-belt-restaurant/).
* [Keys](https://images.app.goo.gl/GNSXWBa2Bfjh3Fen8).
* [Cutting Sound](https://www.youtube.com/watch?v=mFZuY7C4Jvs&t=6s).